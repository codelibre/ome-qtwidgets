# OME Files View

[![pipeline status](https://gitlab.com/codelibre/ome/ome-files-view/badges/master/pipeline.svg)](https://gitlab.com/codelibre/ome/ome-files-view/commits/master)

OME Files View is a simple image viewer for
[OME-TIFF](https://codelibre.gitlab.io/ome/ome-model/doc/ome-tiff/index.html)
image files.

This project was originally developed by the Open Microscopy
Environment in the [ome-qtwidgets GitHub
repository](https://github.com/ome/ome-qtwidgets).  The original
repository has been archived and is not currently under active
development.  This GitLab project is a fork, providing a continuation
of the original project, and is maintained by Codelibre Consulting
Limited.

Documentation
-------------

- [C++ API reference](https://codelibre.gitlab.io/ome/ome-files-view/api/)

Purpose
-------

OME Files View is intended to view OME-TIFF files using the OME Files
C++ library to read the image, and Qt5 and OpenGL to display the image
data and metadata.

Development
-----------

Feature requests, bug reports and improvements should be submitted as
issues or merge requests against this repository.

Codelibre Consulting Limited provides software development consulting
services for this and other projects.  Please [contact
us](mailto:consulting@codelibre.net) to discuss your requirements.

Merge request testing
---------------------

We welcome merge requests from anyone.

Please verify the following before submitting a pull request:

 * verify that the branch merges cleanly into `master`
 * verify that the GitLab pipeline passes
 * verify that the branch only uses C++14 features (this should
   be tested by the pipeline)
 * make sure that your commits contain the correct authorship information and,
   if necessary, a signed-off-by line
 * make sure that the commit messages or pull request comment contains
   sufficient information for the reviewer to understand what problem was
   fixed and how to test it
